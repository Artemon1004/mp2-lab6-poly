﻿//
//  HeadRing.h
//  polynoms
//
//  Created by артем on 10.04.16.
//  Copyright © 2016 артем. All rights reserved.
//
#pragma once
#include "DatList.h"

class THeadRing : public TDatList{
protected:
    PTDatLink pHead;     // заголовок, pFirst - звено за pHead
public:
    THeadRing();
    ~THeadRing();
    // вставка звеньев
    virtual void InsFirst(PTDatValue pVal = NULL); // после заголовка
    // удаление звеньев
    virtual void DelFirst(void);                 // удалить первое звено
};