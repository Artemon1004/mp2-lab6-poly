//
//  DatList.cpp
//  polynoms
//
//  Created by артем on 10.04.16.
//  Copyright © 2016 артем. All rights reserved.
//
#include "../include/DatList.h"

PTDatLink TDatList::GetLink(PTDatValue pVal, PTDatLink pLink) {
    return new TDatLink(pVal, pLink);
}

void TDatList::DelLink(PTDatLink pLink)
{
    if (pLink != NULL)
    {
        if (pLink->pValue != NULL)
            delete pLink->pValue;
        delete pLink;
    }
}

TDatList::TDatList() {
    ListLen=0;
    pFirst = NULL;
    pCurrLink = NULL;
    pLast = NULL;
    Reset();
}

int TDatList::Reset(void) { // установить на начало списка
    pPrevLink = pStop;
    if (!IsEmpty()) {
        pCurrLink = pFirst;
        CurrPos = 0;
    } else {
        pCurrLink = pStop;
        CurrPos = -1;
    }
    return 0;
}

PTDatValue TDatList::GetDatValue(TLinkPos mode) const {
    PTDatLink tmp = NULL;
    switch (mode)
    {
        case FIRST:
            tmp = pFirst;
            break;
        case CURRENT:
            tmp = pCurrLink;
            break;
        case LAST:
            tmp = pLast;
            break;
    }
    if (tmp != NULL)
        return tmp->GetDatValue();
    else
        return NULL;
}

int TDatList::GoNext(void) { // переход к следующему элементу
    if (!IsListEnded()) {
        pPrevLink = pCurrLink;
        pCurrLink = pCurrLink->GetNextDatLink();
        CurrPos++;
        return 1;
    } else {
        return 0;
    }
}


int TDatList::SetCurrentPos(int pos) { // установить текущее звено
    Reset();
    for (int i = 0; i < pos; i++)
        GoNext();
    return 0;
}

int TDatList::GetCurrentPos(void) const { // получить номер тек. звена
    return CurrPos;
}

int TDatList::IsListEnded(void) const { // список завершен ?
    return pCurrLink == pStop;
}

// вставка звеньев

void TDatList::InsFirst(PTDatValue pVal) { // перед первым
    PTDatLink tmp = GetLink(pVal, pFirst);
    if (tmp) {
        tmp->SetNextLink(pFirst);
        pFirst = tmp;
        ListLen++;
        if (ListLen == 1) {
            pLast = tmp;
            Reset();
        }
        else  if (CurrPos == 0) {
            pCurrLink = tmp;
        } else {
            CurrPos++;
        }
    }
    
}

void TDatList::InsLast(PTDatValue pVal) { // вставить последним
    PTDatLink tmp = GetLink(pVal, pStop);
    if (tmp) {
        if (pLast) {
            pLast->SetNextLink(tmp);
        }
        pLast = tmp;
        ListLen++;
        if (ListLen == 1) {
            pFirst = tmp;
            Reset();
        }
        if (IsListEnded()) {
            pCurrLink = tmp;
        }
    }
}

void TDatList::InsCurrent(PTDatValue pVal) { // перед текущим
    if ((pCurrLink == pFirst) || IsEmpty()) {
        InsFirst(pVal);
    } else {
        if (IsListEnded()) {
            InsLast(pVal);
        }
        else {
            PTDatLink tmp = GetLink(pVal, pCurrLink);
            if (tmp) {
                pPrevLink->SetNextLink(tmp);
                tmp->SetNextLink(pCurrLink);
                ListLen++;
                pCurrLink = tmp;
            }
        }
    }
}

// удаление звеньев
void TDatList::DelFirst(void) { // удалить первое звено
    if (!IsEmpty()) {
        PTDatLink tmp = pFirst;
        pFirst = pFirst->GetNextDatLink();
        DelLink(tmp);
        ListLen--;
        if (IsEmpty()) {
            pLast = pStop;
            Reset();
        }
        else if (CurrPos == 1) {
            pCurrLink = pStop;
        } else if (CurrPos == 0) {
            pCurrLink = pFirst;
        }
        if (CurrPos) {
            CurrPos--;
        }
    }
}

void TDatList::DelCurrent(void) { // удалить текущее звено
    if (pCurrLink) {
        if ((pCurrLink == pFirst) || IsEmpty()) {
            DelFirst();
        } else {
            PTDatLink tmp = pCurrLink;
            pCurrLink = pCurrLink->GetNextDatLink();
            pPrevLink->SetNextLink(pCurrLink);
            DelLink(tmp);
            ListLen--;
            if (pCurrLink == pLast) {
                pLast = pPrevLink;
                pCurrLink = pStop;
            }
        }
    }
}

void TDatList::DelList(void) { // удалить весь список
    while (!IsEmpty()) {
        DelFirst();
    }
    CurrPos = -1;
    pFirst = pLast = pPrevLink = pCurrLink = pCurrLink = pStop;
}
