//
//  HeadRing.cpp
//  polynoms
//
//  Created by артем on 10.04.16.
//  Copyright © 2016 артем. All rights reserved.
//
#include "../include/HeadRing.h"

THeadRing::THeadRing() :TDatList() {
    InsLast();
    pHead = pFirst;
    pStop = pHead;
    Reset();
    ListLen = 0;
    pFirst->SetNextLink(pFirst);
}

THeadRing:: ~THeadRing() {
    DelList();
    DelLink(pHead);
    pHead = NULL;
}

void THeadRing::InsFirst(PTDatValue pVal) {
    TDatList::InsFirst(pVal);
    pHead->SetNextLink(pFirst);
}

void THeadRing::DelFirst(void) {
    TDatList::DelFirst();
    pHead->SetNextLink(pFirst);
}