﻿//
//  DatLink.h
//  polynoms
//
//  Created by артем on 10.04.16.
//  Copyright © 2016 артем. All rights reserved.
//
#pragma once
#include "RootLink.h"

class TDatLink;
typedef TDatLink *PTDatLink;

class TDatLink : public TRootLink{
protected:
    PTDatValue pValue;  // указатель на объект значения
public:
    TDatLink(PTDatValue pVal = NULL, PTRootLink pN = NULL) :
    TRootLink(pN) {
        pValue = pVal;
    }
    void       SetDatValue(PTDatValue pVal) { pValue = pVal; }
    PTDatValue GetDatValue() { return  pValue; }
    PTDatLink  GetNextDatLink() { return  (PTDatLink)pNext; }
    friend class TDatList;
    virtual ~TDatLink() {}
};